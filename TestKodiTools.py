#!/usr/bin/python3
import json

from VdrKodiTools.VdrKodiTools import VdrKodiTools
from json import JSONEncoder


def wrapped_default(self, obj):
    return getattr(obj.__class__, "__json__", wrapped_default.default)(obj)


wrapped_default.default = JSONEncoder().default

# apply the patch
JSONEncoder.original_default = JSONEncoder.default
JSONEncoder.default = wrapped_default

vdrKodiTools = VdrKodiTools(True, False)

#vdrKodiTools.dbHub.updateChannelDb()
#vdrKodiTools.svdrpHub.getEpgData('next')
#vdrKodiTools.svdrpHub.getRecordings()
timers = vdrKodiTools.svdrpHub.getTimers()

print(json.dumps(timers))

