#!/bin/bash

for d in $(ls); do
	BACKUPFILE="$d/00001.ts.backup"
	if [[ -f $BACKUPFILE ]]; then
		rm -f $BACKUPFILE
	fi
done
