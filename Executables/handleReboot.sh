#!/bin/bash
REC_COUNT=$(wc -l /var/lib/vdr/recording.lock | cut -d " " -f 1)
CUR_TIME=$(date +%k%M)
if [ "$REC_COUNT" -ne "0" ]; then
	if [ $CUR_TIME -lt 600 ]; then
		sleep 900 && $0
	fi
else
	if [ $CUR_TIME -lt 600 ]; then
		init 6
	fi
fi
