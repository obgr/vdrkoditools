#!/usr/bin/env python
'''
Copyright 2018 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''
from VdrKodiTools import VdrKodiTools
    
def main():
    try:
        vdrKodiTools = VdrKodiTools(False)
    except:
        vdrKodiTools = VdrKodiTools.VdrKodiTools(False)
    
    vdrKodiTools.reloadDvbDrivers(5)

main()
