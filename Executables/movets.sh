#!/bin/bash
FOLDER="Das_Sandmännchen"
SUBFOLDER="Jan_Henry"
RECNAME="Unser_Sandmännchen"

chown -R vdr:vdr $1
mv ../tv/$FOLDER/$SUBFOLDER/$RECNAME/$1/00001.ts ../tv/$FOLDER/$SUBFOLDER/$RECNAME/$1/00001.ts.backup
mv $1/00001.ts ../tv/$FOLDER/$SUBFOLDER/$RECNAME/$1/
rm -rf $1
