#!/usr/bin/env python
'''
Copyright 2018 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''
from VdrKodiTools import VdrKodiTools

vdrKodiTools = None
nextChannel = None



def main():
    try:
        vdrKodiTools = VdrKodiTools(False)
    except:
        vdrKodiTools = VdrKodiTools.VdrKodiTools(False)
    
    vdrKodiTools.switchChannelBeforeRecording(5)

main()
