#!/usr/bin/env python

import sys

f = sys.stdin
while True:
    l = f.readline().strip()
    fields = l.split(" | ") 
    if len(fields) < 2:
        print l
    else:
        # Sig Strength
        sigStr = fields[1].split(" ")[1]
        sig = int(sigStr, 16) / float(0xffff)
        fields[1] = "signal %.1f%%" % (sig * 100.0)
        # BER 
        berStr = fields[3].split(" ")[1]
        ber = int(berStr, 16) 
        fields[3] = "ber %08d" % (ber)
        print " | ".join(fields)
