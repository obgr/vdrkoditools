import json
import logging

class JsonRpcRequestObject():
    def __init__(self, jsonString, handler):
        self.__error = 0

        self.__requestHandler = handler
        logging.error("Incoming json string: %s", jsonString)
        try:
            requestObject = json.loads(jsonString.encode("utf-8"))
            if (("jsonrpc" not in requestObject) or (requestObject.get("jsonrpc") != "2.0")):
                logging.debug("jsonrpc missing")
                self.__error = -32600
                return
            if ("id" in requestObject):
                self.__id = requestObject.get("id")
            else:
                logging.debug("id missing")
                self.__error = -32600
                return
            if ("method" in requestObject):
                self.__method = requestObject.get("method")
            else:
                logging.debug("method missing")
                self.__error = -32600
                return
            self.__params = requestObject.get("params")
            self.__hasAuthToken = False
            if ("auth" in requestObject):
                self.__authToken = requestObject.get("auth")
                self.__hasAuthToken = True
            else:
                logging.debug("auth missing")
                self.__error = -32001
                return
        except ValueError as err:
            logging.error("Error parsing json string: %s", format(err))
            self.__error = -32700


    def hasError(self):
        if (self.__error != 0):
            return True
        return False

    def getError(self):
        return self.__error

    def isLoginConfirmed(self, authToken):
        if (self.__hasAuthToken and authToken == self.__authToken):
            return True
        return False

    def getId(self):
        return self.__id

    def getMethod(self):
        return self.__method

    def getParams(self):
        return self.__params

    def toString(self):
        return ""

    def executeMethod(self):
        if (self.__method != None and hasattr(self.__requestHandler, self.__method)):
            if (self.__params != None):
                return getattr(self.__requestHandler, self.__method)(self.__id, self.__params)
            return getattr(self.__requestHandler, self.__method)(self.__id)
        return getattr(self.__requestHandler, "handleMethodNotFound")()