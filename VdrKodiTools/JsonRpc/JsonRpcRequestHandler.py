import logging
from VdrKodiTools.JsonRpc.JsonRpcRequestObject import JsonRpcRequestObject

class JsonRpcRequestHandler():
    def __init__(self, svdrpHub=None, dbHub=None):
        # Define a static auth token for now...
        self.__authToken = "76dbf15b-b883-40be-a3ca-8c1c8ae1213d"
        self.__errors = {
            -32700: "Parse error",
            -32600: "Invalid request",
            -32601: "Method not found",
            -32602: "Invalid params",
            -32603: "Internal error",
            -32001: "Not authenticated"
        }
        self.svdrpHub = svdrpHub
        self.dbHub = dbHub

    def executeRequest(self, jsonString):
        request = JsonRpcRequestObject(jsonString, self)
        logging.debug("Request String: %s", jsonString)
        if (request.hasError() == False):
            if request.isLoginConfirmed(self.__authToken) == True:
                logging.debug("Execute method")
                return request.executeMethod()
            else:
                logging.debug("Error__0 %i", request.getError())
                return {
                    "jsonrpc": "2.0", "error": self.__createErrorDictionary(request), "id": None
                }
        logging.debug("Error__1 %i", request.getError())
        return {"jsonrpc": "2.0", "error": self.__createErrorDictionary(request), "id": None}

    def __createErrorDictionary(self, request):
        return {"code": request.getError(), "message": self.__errors.get(request.getError())}

    def getChannels(self, jsonRpcId, params=None):
        channels = []
        if params is not None and 'number' in params.keys():
            channels = self.dbHub.getChannelsFromDb(int(params["number"]), None, None)
        elif params is not None and 'name' in params.keys():
            channels = self.dbHub.getChannelsFromDb(None, params["name"], None)
        elif params is not None and 'max' in params.keys():
            channels = self.dbHub.getChannelsFromDb(None, None, int(params["max"]))
        else:
            channels = self.dbHub.getChannelsFromDb()

        if params is not None and 'max' in params.keys():
            channels = channels[:int(params["max"])]

        return {
            "jsonrpc": "2.0", "result": {"channels": channels}, "id": jsonRpcId
        }

    def getTimers(self, jsonRpcId):
        timers = self.svdrpHub.getTimers()

        return {
            "jsonrpc": "2.0", "result": {"timers": timers}, "id": jsonRpcId
        }

    def getRecordings(self, jsonRpcId):
        recordings = self.svdrpHub.getRecordings()

        return {
            "jsonrpc": "2.0", "result": {"recordings": recordings}, "id": jsonRpcId
        }

    def getEpgData(self, jsonRpcId, params):
        epgData = []
        if 'channel' in params.keys():
            epgData = self.svdrpHub.getEpgData(params["channel"], None)
        elif 'time' in params.keys():
            epgData = self.svdrpHub.getEpgData(None, params["time"])
        else:
            epgData = self.svdrpHub.getEpgData(None, None)

        return {
            "jsonrpc": "2.0", "result": {"epgData": epgData}, "id": jsonRpcId
        }

    def createTimer(self, jsonRpcId, params):
        response = self.svdrpHub.createTimer(params)

        return {
            "jsonrpc": "2.0", "result": {"create": response}, "id": jsonRpcId
        }

    def editTimer(self, jsonRpcId, params):
        response = self.svdrpHub.editTimer(params)
        return {
            "jsonrpc": "2.0", "result": {"delete": response}, "id": jsonRpcId
        }

    def removeTimer(self, jsonRpcId, params):
        response = False
        if ("number" in params.keys()):
            response = self.svdrpHub.removeTimer(params["number"])

        return {
            "jsonrpc": "2.0", "result": {"delete": response}, "id": jsonRpcId
        }

    def editRecording(self, jsonRpcId, params):
        response = self.svdrpHub.editRecording(params)

        return {
            "jsonrpc": "2.0", "result": {"delete": response}, "id": jsonRpcId
        }

    def removeRecording(self, jsonRpcId, params):
        response = False
        if ("number" in params.keys()):
            response = self.svdrpHub.removeRecording(params["number"])

        return {
            "jsonrpc": "2.0", "result": {"delete": response}, "id": jsonRpcId
        }

    def login(self, jsonRpcId, params):
        #self.__authToken = str(uuid.uuid4())


        return {
            "jsonrpc": "2.0", "result": {"login": True, "token": self.__authToken}, "id": jsonRpcId
        }

    def logout(self, jsonRpcId, params):

        return {
            "jsonrpc": "2.0", "result": {"logout": True}, "id": jsonRpcId
        }

    def handleMethodNotFound(self):
        return {
            "jsonrpc": "2.0", "error": {"code": -32601, "message": self.__errors.get(-32601)}, "id": None
        }

    def handleInvalidParams(self):
        return {
            "jsonrpc": "2.0", "error": {"code": -32602, "message": self.__errors.get(-32602)}, "id": None
        }