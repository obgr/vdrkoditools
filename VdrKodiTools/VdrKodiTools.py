'''
Copyright 2018 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''

import time
import re
import threading
import logging
import configparser
import requests
from datetime import datetime
from VdrKodiTools.KodiJsonRpc.KodiJsonRpcHub import KodiJsonRpcHub
from VdrKodiTools.Svdrp.SvdrpHub import SvdrpHub
from VdrKodiTools.Db.DbHub import DbHub
from VdrKodiTools.SystemCall.SystemCallHub import SystemCallHub

class VdrKodiTools:
    def __init__(self, systemDebugMode = False, serverDebugMode = False):
        self.currentTimer = None
        self.nextTimer = None
        self.todaysNextTimer = None
        self.playerFile = None
        self.playerStream = None

        self.__debugMode = systemDebugMode

        self.readConfig()
        self.kodiJsonRpcHub = KodiJsonRpcHub(self.kodiUrl, self.kodiClientUrl, self.kodiAuth, serverDebugMode)
        self.svdrpHub = SvdrpHub(self.serverIp, self.serverPort, serverDebugMode)
        self.dbHub = DbHub(self.dbFileLocation, self.svdrpHub, serverDebugMode)
        self.systemCallHub = SystemCallHub(systemDebugMode)

    def startDatabaseUpdater(self):
        print(time.ctime())
        self.dbHub.updateChannelDb()
    
    def readConfig(self):
        config = configparser.ConfigParser()
        if (self.__debugMode == True):
            config.read('VdrKodiTools.conf')
        else:
            config.read('/etc/VdrKodiTools.conf')

        self.kodiUrl = "http://" + config['ConnectionData']['serverIP'] + ":" + config['ConnectionData']['kodiPort'] + "/jsonrpc"
        self.kodiClientUrl = "http://" + config['ConnectionData']['clientIp'] + ":" + config['ConnectionData']['kodiPort'] + "/jsonrpc"
        self.kodiAuth = requests.auth.HTTPBasicAuth(config['KodiAuth']['kodiServerUser'], config['KodiAuth']['kodiServerPassword'])
        self.serverIp = config['ConnectionData']['serverIP']
        self.clientIp = config['ConnectionData']['clientIp']
        self.serverPort = config['ConnectionData']['vdrPort']
        if self.__debugMode == True:
            self.dbFileLocation = "vdrkoditools.db"
        else:
            self.dbFileLocation = config['DB']['dbFileLocation']

    def getNextTimerChannel(self):
        timers = self.svdrpHub.getTimers()
        nextTimerChannel = 11
        timeNow = datetime.now()
        nowDate = str(timeNow.year) + "-" + '{0:02d}'.format(timeNow.month) + "-" + '{0:02d}'.format(timeNow.day)
        if len(timers) > 0:
            self.nextTimer = timers[0]
            if timers[0].date == nowDate:
                self.todaysNextTimer = timers[0]
            else:
                print (timers[0].date + " != " + nowDate)
            dateParts = self.nextTimer.date.split("-")
            dateParts2 = self.nextTimer.date.split("-")
            mTimeStart = self.nextTimer.start
            mTimeEnd = self.nextTimer.end
            if (mTimeEnd < mTimeStart):
                dateParts[2] = int(dateParts[2]) + 1
            timerStart = datetime(int(dateParts[0]), int(dateParts[1]), int(dateParts[2]), int(mTimeStart[:2]), int(mTimeStart[2:]))
            timerEnd = datetime(int(dateParts2[0]), int(dateParts2[1]), int(dateParts2[2]), int(mTimeEnd[:2]), int(mTimeEnd[2:]))
            if timeNow > timerStart and timeNow < timerEnd and len(timers) > 1:
                self.currentTimer = timers[0]
                self.nextTimer = timers[1]
                if timers[1].date == nowDate:
                    self.todaysNextTimer = timers[1]
            else:
                print (timers[1].date + " != " + nowDate)
            nextTimerChannel = self.nextTimer.channelNumber
        myChannels = self.svdrpHub.getChannels(nextTimerChannel)
        if len(myChannels) > 0:
            return myChannels[0].name    
        return None
    
    def rebootWhenNoRecordingIsRunning(self):
        self.getNextTimerChannel()
        if self.currentTimer == None:
            self.systemCallHub.reboot()
    
    def switchChannel(self, nextChannel):
        if nextChannel != None:
            self.kodiJsonRpcHub.sendNotification("Upcoming timer", "Switching channel to "  + nextChannel)
            self.kodiJsonRpcHub.setChannel(nextChannel, 1)
    
    def checkDvBSignalAndSwitchChannel(self, nextChannel):
        dvbStrengthSufficient = self.systemCallHub.getDvbStatus(0) and self.systemCallHub.getDvbStatus(1)
        if dvbStrengthSufficient == False:
            self.kodiJsonRpcHub.stopPlayback()
            print("Reload dvb drivers at: " + datetime.now().isoformat())
            self.systemCallHub.reloadDrivers()
            counter = 0
            while (self.systemCallHub.getDvbStatus(0) == False or self.systemCallHub.getDvbStatus(1)) == False and counter < 10:
            #while mDvbStatus == False and counter < 3:
                time.sleep(3)
                counter += 1
            if (counter < 10):
                #if True:
                self.switchChannel(nextChannel)
        else:
            #self.switchChannel(nextChannel)
            pass

    def switchChannelBeforeRecording(self, cronInterval):
        nextChannel = self.getNextTimerChannel()
        #print(nextChannel)
        if self.todaysNextTimer != None and (self.currentTimer == None or self.todaysNextTimer.start != self.currentTimer.end):
            x = datetime.now()
            mHours = int(self.todaysNextTimer.start[:2])
            mMinutes = int(self.todaysNextTimer.start[2:])
            if (mMinutes == 0):
                if mHours == 0:
                    mHours = 23
                else:
                    mHours = mHours - 1
                mMinutes = 59
            else:
                mMinutes = mMinutes - 1
            dateParts = self.todaysNextTimer.date.split("-")
            y = datetime(int(dateParts[0]), int(dateParts[1]), int(dateParts[2]), mHours, mMinutes, 0)
            #print(y)
            delta_t = y - x

            secs = delta_t.seconds + 1
            #print("***** compare time *****")
            #print(secs)
            #print(cronInterval * 60)
            #secs = 5
            if secs <  cronInterval * 60:
                t = threading.Timer(secs, self.checkDvBSignalAndSwitchChannel, [nextChannel])
                print("Start timer")
                t.start()

    def reloadDvbDrivers(self, cronInterval):
        self.switchChannelBeforeRecording(cronInterval)
        channel = self.getNextTimerChannel()
        if channel == None:
            channel = "KiKA"
        dvbStrengthSufficient = True
        if self.currentTimer == None:
            playerStatus = self.kodiJsonRpcHub.getPlayerStatus()
            if (playerStatus == False):
                dvbStrengthSufficient = self.getDvbStatus(0) and self.getDvbStatus(1)
                if dvbStrengthSufficient == False:
                    print("Reload dvb drivers at: " + datetime.now().isoformat())
                    self.reloadDrivers()
                    counter = 0    
                    mDvbStatus = self.getDvbStatus(0) and self.getDvbStatus(1)            
                    while mDvbStatus == False and counter < 10:
                    #while mDvbStatus == False and counter < 3:
                        time.sleep(3)
                        counter += 1
                    if (counter < 10):
                        #if True:
                        self.kodiJsonRpcHub.setChannel(channel, 1)
                        print("Set channel to: " + channel)
                else:
                    #self.setChannel(channel, 1)
                    print("No dvb drivers reload at: " + datetime.now().isoformat())
                    #print("Set channel to: " + channel)
            else:
                #print("Player running")            
                pass
        else:
            #print("Current timer running")
            pass
