'''
Copyright 2020 - 2024 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''

import sqlite3
from VdrKodiTools.Model.Channel import Channel

class DbHub:
    def __init__(self, dbFileLocation, svdrpHub, debugMode = False):
        self.svdrpHub = svdrpHub
        self.dbFileLocation = dbFileLocation
        self.__debugMode = debugMode

        self.__createDb()
    
    def __createDb(self):
        conn = sqlite3.connect(self.dbFileLocation)
        c = conn.cursor()

        c.execute('''
            CREATE TABLE IF NOT EXISTS CHANNEL 
            (
                NUMBER int, CHANNELNAME text, CHANNELID text, CHANNELGROUP text
            )
            ''')
        conn.commit()
        conn.close()

    def updateChannelDb(self):
        channelList = self.svdrpHub.getChannels()

        conn = sqlite3.connect(self.dbFileLocation)
        c = conn.cursor()

        c.execute('''DELETE FROM CHANNEL WHERE 1 = 1''')
        for channel in channelList:
            c.execute('''
                INSERT INTO CHANNEL
                    (NUMBER, CHANNELNAME, CHANNELID, CHANNELGROUP)
                    VALUES
                    (?, ?, ?, ?)
            ''', (channel.getChannelNumber(), channel.getChannelName(), channel.getId(), channel.getGroup()))
        conn.commit()
        conn.close()

        return channelList
    
    def getChannelsFromDb(self, number=None, name=None, max=None):
        sqlString = "SELECT * FROM CHANNEL"
        if number is not None:
            sqlString += " WHERE NUMBER = " + str(number)
        elif name is not None:
            sqlString += " WHERE CHANNELNAME = " + name
        if max is not None:
            sqlString += " WHERE NUMBER <= " + str(max)
        sqlString += " ORDER BY NUMBER"

        conn = sqlite3.connect(self.dbFileLocation)
        c = conn.cursor()
        c.execute(sqlString)
        rows = c.fetchall()
        conn.close()

        
        channels = []
        for aRow in rows:
            aChannel = Channel(None, aRow)
            channels.append(aChannel)
        return channels

