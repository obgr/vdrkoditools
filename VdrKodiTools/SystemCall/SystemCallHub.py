'''
Copyright 2020 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''

import shlex
import os
from subprocess import Popen, PIPE

class SystemCallHub:
    def __init__(self, debugMode=False):
        self.debugMode = debugMode
        
    def getDvbStatus(self, dvbAdapter):
        if self.debugMode == True:
            return True
        process = Popen(shlex.split("femon -H -c 2 -a " + dvbAdapter), stdout=PIPE, stderr=PIPE)
        (output, err) = process.communicate()
        exit_code = process.wait()
        
        signalStrengths = []
        averageStrength = 0
        outputList = output.splitlines()
        for line in outputList:
            if line.startswith("status"):
                parts = line.split("|")
                tmp = parts[1].strip().replace("signal", "").replace("%", "").strip()
                signalStrengths.append(int(tmp))
        sumStrength = 0
        for strength in signalStrengths:
            sumStrength += strength
        if len(signalStrengths) > 0:
            averageStrength = sumStrength / len(signalStrengths)
        
        if averageStrength > 5:
            return True
        else:
            return False

    def reloadDrivers(self):
        if self.debugMode == True:
            return
        #print("Reload drivers")
        process = Popen(shlex.split("/usr/local/bin/reloadDvbDrivers.sh"), stdout=PIPE, stderr=PIPE)
        (output, err) = process.communicate()
        exit_code = process.wait()
    
    def reboot(self):
        if self.debugMode == True:
            return
        #print("Rebooting the system")
        process = Popen(shlex.split("reboot"), stdout=PIPE, stderr=PIPE)
        (output, err) = process.communicate()
        exit_code = process.wait()

    def restartKodi(self, id):
        retVal = os.system("/bin/sh restartKodi.sh")
        return {"jsonrpc": "2.0", "result": {"restart": retVal == 0}, "id": id}