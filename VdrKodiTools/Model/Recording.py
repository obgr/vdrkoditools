'''
Copyright 2018, 2020 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''

import json
from VdrKodiTools.Model.BaseData import BaseData

class Recording(BaseData):
    def __init__(self, rawData):
        dataParts = rawData.split(" ")
        self.__number = self.getNumber(dataParts[0])
        self.__startDate = self.getUnicodeString(dataParts[1])
        if (dataParts[2].endswith("*")):
            self.__startTime = self.getUnicodeString(dataParts[2][:-1])
            self.__isNew = True
        else:
            self.__startTime = self.getUnicodeString(dataParts[2])
            self.__isNew = False
        self.__name = self.getUnicodeString(" ".join(dataParts[4:]))

    def toDictionary(self):
        return {
            "number": self.__number,
            "startDate": self.__startDate,
            "startTime": self.__startTime,
            "name": self.__name,
            "isNew": self.__isNew
        }

    def __json__(self):
        return json.dumps(self.toDictionary(), indent=4)
