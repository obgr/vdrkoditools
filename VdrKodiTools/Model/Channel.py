'''
Copyright 2018, 2020 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''

import json
#import logging
from VdrKodiTools.Model.BaseData import BaseData

class Channel(BaseData):
    def __init__(self, rawData, dbData=None):
        self.__number = None
        self.__name = None
        self.__id = None
        self.__group = None
        if rawData is not None:
            parts = rawData.split(" ")
            tmpNumber = self.getNumber(parts[0])
            rawData = " ".join(parts[1:])
            parts = rawData.split(":")
            if (len(parts) > 12):
                self.__number = tmpNumber
                self.__name = self.getUnicodeString(parts[0].split(";")[0])
                # Das Erste;ARD:11836:HC34M2S0:S19.2E:27500:101=2:102=deu@3,103=mis@3;106=deu@106:104;105=deu:0:28106:1:1101:0
                # S19.2E-1-1101-28106
                channelId = parts[3] + "-" + parts[10] + "-" + parts[11] + "-" + parts[9]
                if (self.getNumber(parts[12]) != 0):
                    channelId += parts[12]
                self.__id = self.getUnicodeString(channelId)
                self.__group = ''
        if dbData is not None:
            self.__number = dbData[0];
            self.__name = dbData[1];
            self.__id = dbData[2];
            self.__group = dbData[3];
        self.name = self.__name

    def getId(self):
        return self.__id
    
    def setId(self, channelId):
        self.__id = channelId

    def getChannelName(self):
        return self.__name
    
    def setChannelName(self, name):
        self.__name = name

    def getGroup(self):
        return self.__group
    
    def setGroup(self, group):
        self.__group = group
    
    def getChannelNumber(self):
        return self.__number
    
    def setChannelNumber(self, number):
        self.__number = number

    def toDictionary(self):
        return {
            "number": self.__number,
            "name": self.__name,
            "id": self.__id,
            "group": self.__group
        }

    def __json__(self):
        return json.dumps(self.toDictionary(), indent=4)