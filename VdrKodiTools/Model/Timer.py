'''
Copyright 2018,2020 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''

import json
from VdrKodiTools.Model.BaseData import BaseData

class Timer(BaseData):
    def __init__(self, rawData):
        parts = rawData.split(" ")
        self.__number = self.getNumber(parts[0])
        rawData = " ".join(parts[1:])
        parts = rawData.split(":")
        self.__isActive = False
        self.__isVps = False
        if (self.getNumber(parts[0]) == 1):
            self.__isActive = True
        if (self.getNumber(parts[0]) == 5):
            self.__isActive = True
            self.__isVps = True
        self.__channel = self.getNumber(parts[1])
        self.__date = self.getUnicodeString(parts[2])
        self.__start = self.getUnicodeString(parts[3])
        self.__end = self.getUnicodeString(parts[4])
        self.__priority = self.getNumber(parts[5])
        self.__lifeTime = self.getNumber(parts[6])
        self.__name = self.getUnicodeString(parts[7])
        self.__description = self.getUnicodeString(parts[8])
        self.date = self.__date
        self.channelNumber = self.__channel
        self.start = self.__start
        self.end = self.__end

    def getTimerNumber(self):
        return self.__number

    def toDictionary(self):
        return {
            "number": self.__number,
            "isActive": self.__isActive,
            "isVps": self.__isVps,
            "channel": self.__channel,
            "date": self.__date,
            "start": self.__start,
            "end": self.__end,
            "priority": self.__priority,
            "lifeTime": self.__lifeTime,
            "name": self.__name,
            "description": self.__description
        }

    def __json__(self):
        return json.dumps(self.toDictionary(), indent=4)
