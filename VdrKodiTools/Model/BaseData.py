'''
Copyright 2018 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''
class BaseData():
    def getUnicodeString(self, inputData):
        if (not isinstance(inputData, str)):
            mUnicodeString = ""
            try:
                mUnicodeString = inputData.decode(encoding='utf-8', errors='strict') # str(inputData, "utf-8")
            except:
                mUnicodeString = "Error reading input data"
            return mUnicodeString
        return inputData

    def getNumber(self, inputData):
        mNumber = 0
        try:
            mNumber = int(inputData)
        except ValueError:
            pass
        return mNumber

    def getName(self, inputData):
        if (not isinstance(inputData, str)):
            mUnicodeString = ""
            try:
                mUnicodeString = inputData.decode(encoding='utf-8', errors='strict') # str(inputData, "utf-8")
            except:
                pass
            parts = mUnicodeString.split("-")
        else:
            parts = inputData.split("-")
        if len(parts) > 1:
            return parts[1].strip()
        return ""
