'''
Copyright 2018, 2020 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''

import json
from VdrKodiTools.Model.BaseData import BaseData

class EpgData(BaseData):
    def __init__(self, channel, rawData):
        self.__channel = {"id": self.getUnicodeString(channel[0]), "name": self.getUnicodeString(channel[1])}
        self.__title = ""
        self.__shortText = ""
        self.__description = ""
        self.__vpsStart = None
        for i in range(len(rawData)):
            if (rawData[i].startswith("E")):
                parts = rawData[i].split()
                self.__id = self.getNumber(parts[1])
                self.__start = self.getNumber(parts[2])
                self.__duration = self.getNumber(parts[3])
            elif (rawData[i].startswith("T")):
                self.__title = self.getUnicodeString(rawData[i][2:])
            elif (rawData[i].startswith("S")):
                self.__shortText = self.getUnicodeString(rawData[i][2:])
            elif (rawData[i].startswith("D")):
                self.__description = self.getUnicodeString(rawData[i][2:])
            elif (rawData[i].startswith("V")):
                self.__vpsStart = self.getNumber(rawData[i][2:])

        #self.__genre = 0
        #self.__parentalRating = 0
    def getId(self):
        return self.__id

    def getChannel(self):
        return self.__channel

    def getTitle(self):
        return self.__title

    def getShortText(self):
        return self.__shortText

    def getDescription(self):
        return self.__description

    def getStart(self):
        return self.__start

    def getDuration(self):
        return self.__duration

    def getVpsStart(self):
        return self.__vpsStart

    def toDictionary(self):
        return {
            "channel": self.__channel,
            "id": self.__id,
            "start": self.__start,
            "duration": self.__duration,
            "title": self.__title,
            "shortText": self.__shortText,
            "description": self.__description,
            "vpsStart": self.__vpsStart
        }

    def __json__(self):
        return json.dumps(self.toDictionary(), indent=4)
