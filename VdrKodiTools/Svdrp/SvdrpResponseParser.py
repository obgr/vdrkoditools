from VdrKodiTools.Model.Channel import Channel
from VdrKodiTools.Model.EpgData import EpgData
from VdrKodiTools.Model.Recording import Recording
from VdrKodiTools.Model.Timer import Timer

class SvdrpResponseParser():

    def __init__(self):
        pass

    def parse(self, requestType, response):
        if (requestType == "LSTC"):
            return self.__parseChannelList(response)
        elif(requestType == "LSTT"):
            return self.__parseTimerList(response)
        elif(requestType == "LSTE"):
            return self.__parseEpgList(response)
        elif(requestType == "LSTR"):
            return self.__parseRecordingList(response)
        return None

    def __parseChannelList(self, response):
        channelList = []
        for i in range(len(response)):
            channelList.append(Channel(response[i]))
        return channelList

    def __parseTimerList(self, response):
        timerList = []
        for i in range(len(response)):
            timerList.append(Timer(response[i]))
        return timerList

    def __parseEpgList(self, response):
        epgList = []
        epgRawData = []
        channelId = None
        channelName = None
        for i in range(len(response)):
            typeCode = response[i].split()[0]
            if (typeCode == "C"):
                splitResponse = response[i].split()
                channelId = splitResponse[1]
                if (len(splitResponse) > 2):
                    channelName = " ".join(splitResponse[2:])
            if (typeCode == "E"):
                epgRawData = []
            if (typeCode != "C" and typeCode != "c" and typeCode != "e"):
                epgRawData.append(response[i])
            if (typeCode == "e"):
                epgList.append(EpgData((channelId, channelName), epgRawData))
        return epgList

    def __parseRecordingList(self, response):
        recordingList = []
        for i in range(len(response)):
            recordingList.append(Recording(response[i]))
        return recordingList