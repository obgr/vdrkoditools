'''
Copyright 2018, 2020 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''

import socket
import re
import time
import logging
from VdrKodiTools.Svdrp.SvdrpResponseParser import SvdrpResponseParser

def default(o):
    if hasattr(o, "toDictionary"):
        return o.toDictionary()
    else:
        return o

class SvdrpHub():

    def __init__(self, serverIp, serverPort, debugMode=False):
        self.__serverIp = serverIp
        self.__serverPort = int(serverPort)

        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__parser = SvdrpResponseParser()
        self.__debugMode = debugMode

    def getChannels(self, number=None, name=None):
        if (self.__debugMode == False):
            requestString = "LSTC"
            if (number is not None):
                requestString += " " + str(number)
            elif (name is not None):
                requestString += " " + name
            response = self.__sendSvdrpRequest(requestString, 2)
        else:
            response = self.__readDummyFile("channels.dummy.txt")
        channels = []
        for i in range(len(response)):
            #with open("channels.dummy.txt", "a") as dummyfile:
            #    dummyfile.write(response[i] + "\n")
            if response[i].startswith("250-") or response[i].startswith("250 "):
                channels.append(response[i][4:])
        return self.__parser.parse("LSTC", channels)

    def getTimers(self):
        if (self.__debugMode == False):
            response = self.__sendSvdrpRequest("LSTT", 2)
        else:
            response = self.__readDummyFile("timer.dummy.txt")
        timers = []
        for i in range(len(response)):
            #with open("timer.dummy.txt", "a") as dummyfile:
            #    dummyfile.write(response[i] + "\n")
            if response[i].startswith("250-") or response[i].startswith("250 "):
                timers.append(response[i][4:])
        return self.__parser.parse("LSTT", timers)

    def getEpgData(self, channel=None, epgTime=None):
        if (self.__debugMode == False):
            if (channel is not None):
                response = self.__sendSvdrpRequest("LSTE " + str(channel))
            else:
                if (epgTime == "now"):
                    response = self.__sendSvdrpRequest("LSTE now")
                elif(epgTime == "next"):
                    response = self.__sendSvdrpRequest("LSTE next")
                elif epgTime is not None:
                    response = self.__sendSvdrpRequest("LSTE at " + time)
        else:
            if (channel is not None):
                response = self.__readDummyFile("epg_ard.dummy.txt")
            else:
                if (epgTime == "now"):
                    response = self.__readDummyFile("epg_now.dummy.txt")
                elif(epgTime == "next"):
                    response = self.__readDummyFile("epg_next.dummy.txt")
                elif epgTime is not None:
                    response = self.__readDummyFile("epg_now.dummy.txt")
        epgData = []
        for i in range(len(response)):
            #with open("epg_next.dummy.txt", "a") as dummyfile:
            #    dummyfile.write(response[i] + "\n")
            if (response[i].startswith("215-")):
                epgData.append(response[i][4:])

        return self.__parser.parse("LSTE", epgData)

    def getRecordings(self):
        if (self.__debugMode == False):
            response = self.__sendSvdrpRequest("LSTR", 2)
        else:
            response = self.__readDummyFile("recording.dummy.txt")
        recordings = []
        for i in range(len(response)):
            #with open("recording.dummy.txt", "a") as dummyfile:
            #    dummyfile.write(response[i] + "\n")
            if response[i].startswith("250-") or response[i].startswith("250 "):
                recordings.append(response[i][4:])
        return self.__parser.parse("LSTR", recordings)

    def createTimer(self, params):
        if (self.__debugMode == False):
            if ("isActive" in params.keys() and "channel" in params.keys() and "day" in params.keys() and "start" in params.keys() and "end" in params.keys() and "prio" in params.keys() and "lifeTime" in params.keys() and "title" in params.keys()):
                requestString = "NEWT " + str(params["isActive"]) + ":" + str(params["channel"]) + ":" + params["day"] + ":"
                requestString += params["start"] + ":" + params["end"] + ":" + str(params["prio"]) + ":"
                requestString += str(params["lifeTime"]) + ":" + params["title"] + ":"
                self.__sendSvdrpRequest(requestString, 2)
                return True
        else:
            return True
        return False

    def editTimer(self, params):
        if (self.__debugMode == False):
            requestString = ""
            if ("number" in params.keys()):
                if ("activeState" in params.keys()):
                    requestString = "MODT " + params["number"] + " " + params["activeState"]
                elif ("isActive" in params.keys() and "channel" in params.keys() and "day" in params.keys() and "start" in params.keys() and "end" in params.keys() and "prio" in params.keys() and "lifeTime" in params.keys() and "title" in params.keys()):
                    requestString = "MODT " + str(params["number"]) + " " + str(params["isActive"]) + ":" + str(params["channel"]) + ":" + params["day"] + ":"
                    requestString += params["start"] + ":" + params["end"] + ":" + str(params["prio"]) + ":"
                    requestString += str(params["lifeTime"]) + ":" + params["title"] + ":"
                self.__sendSvdrpRequest(requestString, 2)
                return True
        else:
            return True
        return False

    def removeTimer(self, timerNumber):
        if (self.__debugMode == False):
            requestString = "DELT " + str(timerNumber)
            self.__sendSvdrpRequest(requestString, 2)
        return True

    def editRecording(self, params):
        return False

    def removeRecording(self, recordingNumber):
        if (self.__debugMode == False):
            requestString = "DELR " + str(recordingNumber)
            self.__sendSvdrpRequest(requestString, 2)
        return True

    def __sendSvdrpRequest(self, requestString, timeout = 5):
        try:
            requestBytes = (requestString + "\n").encode(encoding='utf-8', errors='strict')
            self.__socket.connect((self.__serverIp, self.__serverPort))
            self.__socket.sendall(requestBytes)
        except Exception as error:
            print(error)
        textResponse = self.__recvCompleteMessage(timeout)
        self.__socket.close()
        return self.__splitAtLineBreaks(textResponse)

    def __recvCompleteMessage(self, timeout):
        self.__socket.setblocking(0)
        totalData = []
        begin = time.time()
        lastString = ''
        data = ''
        while True:
            if totalData and time.time() - begin > timeout:
                break
            elif time.time() - begin > timeout * 2:
                break
            try:
                data = self.__socket.recv(4096).decode(encoding='utf-8', errors='strict')
                if data:
                    totalData.append(data)
                    begin = time.time()
                else:
                    data = ''
                    time.sleep(0.01)
            except:
                data = ''
                time.sleep(0.1)
            testString = lastString + data
            if self.__testPatternMatch(testString, r"\n[1-9][0-9]{2} "):
                logging.error('Match found')
                break
            lastString = data
        return ''.join(totalData)

    def __testPatternMatch(self, testString, regEx):
        return bool(re.search(regEx, testString))

    def __splitAtLineBreaks(self, data):
        tmpData = re.sub(r"\r\n", "\n", data)
        tmpData2 = re.sub(r"\r", "\n", tmpData)
        parts = tmpData2.split("\n")
        return parts
    
    def __readDummyFile(self, dummyFileName):
        with open(dummyFileName, "r") as dummyFile:
            lines = dummyFile.readlines()
        lines = [l.strip() for l in lines]
        return lines
