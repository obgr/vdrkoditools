'''
Copyright 2020 Oliver Burger

This file is part of VdrKodiTools, which is under MIT license
'''

from random import randint
import json
import requests

class KodiJsonRpcHub():
    def __init__(self, kodiUrl, kodiClientUrl, kodiAuth, debugMode=False):
        self.kodiUrl = kodiUrl
        self.kodiClientUrl = kodiClientUrl
        self.kodiAuth = kodiAuth

        self.__debugMode = debugMode
    
    def __createJsonRequestString(self, rpcId, method, params):
        requestObject = {
            "jsonrpc": "2.0",
            "method": method,
            "id": rpcId}
        if (params is not None):
            requestObject["params"] = params
        jsonRequestString = json.dumps(requestObject)
        return jsonRequestString
    
    def __sendKodiJsonRpcRequest(self, method, params=None, resultExpected=True):
        rpcId = randint(1000,9999)
        content = self.__createJsonRequestString(rpcId, method, params)
        r = requests.post(self.kodiUrl, data = content, auth = self.kodiAuth)
        if resultExpected:
            response = json.loads(r.text)
            result = response.get("result", None)

            return result

    def getPlayerStatus(self):
        result = self.__sendKodiJsonRpcRequest("Player.GetActivePlayers")
        playerId = -1
        if (result is not None and len(result) > 0):
            playerId = result[0].get("playerid", -1)
        else:
            print("No playerId")
            return False
        
        if (playerId != -1):
            '''result = self.__sendKodiJsonRpcRequest("Player.GetProperties", 
                {"playerid": playerId, "properties": ["speed"]})'''
            
            playerFileAndStream = self.getPlayerFileAndStream(playerId)
            if (playerFileAndStream[0] is not None and playerFileAndStream[0].startswith('pvr://')) or playerFileAndStream[1] is not None:
                print(playerFileAndStream)
                return True
            else:
                print(playerFileAndStream)
                print(playerFileAndStream[0] is not None)
                if playerFileAndStream[0] is not None:
                    print(playerFileAndStream[0].startswith('pvr://'))
                print(playerFileAndStream[1] is not None)
        print("No player running")
        return False
    
    def getPlayerFileAndStream(self, playerId):
        result = self.__sendKodiJsonRpcRequest("Player.GetItem", {"playerid": playerId, "properties": ["file", "streamdetails"]})
        mFile = None
        mStreamType = None
        if (result is not None):
            item = result.get("item", None)
            if item is not None:
                mFile = item.get("file", None)
                mStreamType = item.get("type", None)
        return [mFile, mStreamType]          

    def setChannel(self, chName, groupId):
        result = self.__sendKodiJsonRpcRequest("PVR.GetChannels", {"channelgroupid": groupId})
        if (result is not None):
            channels = result.get("channels", [])
            for ch in channels:
                chLabel = ch.get("label", "")
                chId = ch.get("channelid", -1)
                if (chLabel == chName and chId != -1):
                    self.__sendKodiJsonRpcRequest("Player.Open", {"item": {"channelid": chId}}, False)

    def sendNotification(self, lTitle, lText):
        self.__sendKodiJsonRpcRequest("GUI.ShowNotification", {"title": lTitle, "message": lText, "displaytime": 3000}, False)
    
    def stopPlayback(self):
        result = self.__sendKodiJsonRpcRequest("Player.GetActivePlayers")
        if (result is not None and len(result) > 0):
            playerId = result[0].get("playerid", -1)
        if playerId != -1:
            self.__sendKodiJsonRpcRequest("Player.Stop", {"playerid": playerId}, False)
