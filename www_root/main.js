$(document).ready(function(){
    $("#channelsend").click(function(){
        var jsonRpcData = '{"jsonrpc": "2.0", "method": "getChannels", "params": {"max": 40}, "auth": "76dbf15b-b883-40be-a3ca-8c1c8ae1213d", "id": 1234}';
        $.ajax({
            url: ".", 
            type: "POST", 
            data: jsonRpcData,
            dataType: "json",
            success: (response) => {
                $("#result").text(JSON.stringify(response.result.channels));
            },
            error: (jqXHR, textStatus, errorThrown) => {
                $("#result").text(errorThrown);
            }
        });
    });
    $("#epgsend").click(function(){
        var jsonRpcData = '{"jsonrpc": "2.0", "method": "getEpgData", "params": {"time": "now"}, "auth": "76dbf15b-b883-40be-a3ca-8c1c8ae1213d", "id": 1234}';
        $.ajax({
            url: ".", 
            type: "POST", 
            data: jsonRpcData,
            dataType: "json",
            success: (response) => {
                $("#result").text(JSON.stringify(response.result.epgData));
            },
            error: (jqXHR, textStatus, errorThrown) => {
                $("#result").text(errorThrown);
            }
        });
    });
    $("#recordingsend").click(function(){
        var jsonRpcData = '{"jsonrpc": "2.0", "method": "getRecordings", "auth": "76dbf15b-b883-40be-a3ca-8c1c8ae1213d", "id": 1234}';
        $.ajax({
            url: ".", 
            type: "POST", 
            data: jsonRpcData,
            dataType: "json",
            success: (response) => {
                $("#result").text(JSON.stringify(response.result.recordings));
            },
            error: (jqXHR, textStatus, errorThrown) => {
                $("#result").text(errorThrown);
            }
        });
    });
    $("#timersend").click(function(){
        var jsonRpcData = '{"jsonrpc": "2.0", "method": "getTimers", "auth": "76dbf15b-b883-40be-a3ca-8c1c8ae1213d", "id": 1234}';
        $.ajax({
            url: ".", 
            type: "POST", 
            data: jsonRpcData,
            dataType: "json",
            success: (response) => {
                $("#result").text(JSON.stringify(response.result.timers));
            },
            error: (jqXHR, textStatus, errorThrown) => {
                $("#result").text(errorThrown);
            }
        });
    });
  });