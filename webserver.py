#!/usr/bin/python3

import http.server
import json
import os
import socket
import logging
import html
import sys
from functools import partial

from VdrKodiTools.JsonRpc.JsonRpcRequestHandler import JsonRpcRequestHandler
from VdrKodiTools.VdrKodiTools import VdrKodiTools

def default(o):
    if hasattr(o, "toDictionary"):
        return o.toDictionary()
    else:
        return o

def checkIfPortIsUsed(portNumber):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    portIsFree = False
    try:
        s.bind(("127.0.0.1", portNumber))
        portIsFree = True
    except:
        pass
    s.close()
    return portIsFree

class HttpRequestHandler(http.server.BaseHTTPRequestHandler):

    def __init__(self, systemDebug, serverDebug, *args, **kwargs):
        self.systemDebug = systemDebug
        self.serverDebug = serverDebug
        super().__init__(*args, **kwargs)

    def do_HEAD(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_GET(self):
        logging.debug("Handling GET request")
        #self.send_response(200)
        #self.send_header("Content-type", "text/html")
        #self.end_headers()
        dirPath = os.path.dirname(os.path.realpath(__file__)) + "/www_root"
        path = dirPath + self.path
        if os.path.exists(path) and os.path.isdir(path):
            indexFiles = ['index.html', 'index.htm', ]
            for indexFile in indexFiles:
                tmpPath = path + indexFile
                if os.path.exists(tmpPath):
                    path = tmpPath
                    break
        
        logging.debug("GET: requested path: %s", path)
        if os.path.exists(path) and os.path.isfile(path):
            _, ext = os.path.splitext(path)
            ext = ext.lower()
            content_type = {
                '.css': 'text/css',
                '.gif': 'image/gif',
                '.htm': 'text/html',
                '.html': 'text/html',
                '.jpeg': 'image/jpeg',
                '.jpg': 'image/jpg',
                '.js': 'text/javascript',
                '.map': 'application/json',
                '.json': 'application/json',
                '.ico': 'image/x-icon',
                '.png': 'image/png',
                '.text': 'text/plain',
                '.txt': 'text/plain',
            }

            if ext in content_type:
                self.send_response(200)  # OK
                self.send_header('Content-type', content_type[ext])
                self.end_headers()
                httpResponse = ''
                with open(path) as ifp:
                    httpResponse += ifp.read()
                logging.debug("GET: Send 200 OK reply with data")
                self.wfile.write(httpResponse.encode("utf-8"))
            else:
                self.send_response(403)  # Forbidden
                self.send_header('Content-type', "text/plain")
                self.end_headers()
                logging.debug("GET: Send 403 Forbidden reply")
                self.wfile.write("Forbidden".encode("utf-8"))
        else:
            self.send_response(404)  # Not Found
            self.send_header('Content-type', "text/plain")
            self.end_headers()
            logging.debug("GET: Send 404 Not Found reply")
            self.wfile.write("Not Found".encode("utf-8"))

    def do_POST(self):
        logging.debug("Handling POST request")
        resultString = ""
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
        requestLength = 0
        if hasattr(self.headers, 'getheader'):
            requestLength = int(self.headers.getheader('content-length'))
        else:
            requestLength = int(self.headers.get('content-length'))
        logging.debug("POST: Got request with length: %s", str(requestLength))
        requestString = self.rfile.read(requestLength)
        requestString = html.unescape(str(requestString, "ascii"))
        logging.debug("POST: Handling request: %s", requestString)

        vdrKodiTools = VdrKodiTools(self.systemDebug, self.serverDebug)
        requestHandler = JsonRpcRequestHandler(vdrKodiTools.svdrpHub, vdrKodiTools.dbHub)
        requestResult = requestHandler.executeRequest(requestString)

        resultString = json.dumps(requestResult, default=default)
        logging.debug("POST: Send 200 OK reply with data")
        self.wfile.write(resultString.encode("utf-8"))

def httpd(systemDebug, serverDebug):
    portToUse = 8888
    while checkIfPortIsUsed(portToUse) == False:
        portToUse += 1

    vdrKodiTools = VdrKodiTools(systemDebug, serverDebug)
    vdrKodiTools.startDatabaseUpdater()
    handler = partial(HttpRequestHandler, systemDebug, serverDebug)
    server = http.server.HTTPServer(('', portToUse), handler)
    try:
        logging.debug("Starting webserver at port %s", str(portToUse))
        print("Starting webserver at port " + str(portToUse))
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    logging.debug("Stopping webserver on port: %s", str(portToUse))
    server.server_close()

def main():
    args = sys.argv
    systemDebug = False
    serverDebug = False
    if len(args) > 1 and args[1] == "debug":
        systemDebug = True
    if len(args) > 1 and args[2] == "debug":
        serverDebug = True
    logging.basicConfig(filename='example.log',level=logging.DEBUG)
    httpd(systemDebug, serverDebug)

if __name__ == '__main__':
    main()
